from setuptools import find_packages, setup

import twelve

with open('README.md') as f:
    long_description = f.read()

setup(
    name='twelve',
    version=twelve.__version__,
    author='Vivian Hellyer',
    author_email='vhellyer@tristratagroup.com',
    description='Utilities for working with Python.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/tristratagroup/twelve',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
