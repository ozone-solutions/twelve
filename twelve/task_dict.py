from asyncio import CancelledError, Task
import contextlib
from typing import Dict, TypeVar

T = TypeVar('T')


class TaskDict(Dict[T, Task]):
    """A dictionary of asyncio tasks.

    Operations such as clearing and deleting will cancel the existing task if
    it exists.

    Setting a key with a new task will cancel the previous task, if it
    existed.
    """

    def clear(self) -> None:
        """Cancels and clears all tasks from the dict.

        This will immediately cancel all tasks and then clear them."""

        for task in self.values():
            with contextlib.suppress(CancelledError):
                task.cancel()

        super().clear()

    def __delitem__(self, k: T) -> None:
        """Deletes and cancels a task."""

        with contextlib.suppress(CancelledError, KeyError):
            self[k].cancel()

        super().__delitem__(k)

    def __setitem__(self, k: T, v: Task) -> None:
        """Replaces a key with a new value.

        This cancels the task of the previous value if there was one."""

        with contextlib.suppress(CancelledError, KeyError):
            self[k].cancel()

        super().__setitem__(k, v)
