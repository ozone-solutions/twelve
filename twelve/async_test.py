import asyncio
import typing
from typing import Any, Callable, Coroutine


def async_test(f: Coroutine[object, Any, Any]) -> Callable[[object], None]:
    """Synchronously runs an asynchronous function, intended to be a test.

    This decorator is intended to be run with `unittest.TestCase` tests that
    need to be asynchronous, but run synchronously on the event loop.

    Examples:
        >>> import asyncio
        >>> from unittest import TestCase
        >>> from twelve import async_test
        >>> class MyTest(TestCase):
        ...     @async_test
        ...     async def test_something(self) -> None:
        ...         await asyncio.sleep(0.5)
        ...         # Other async stuff here.
    """

    def decorator(*args, **kwargs) -> None:
        coroutine = asyncio.coroutine(typing.cast(Any, f))
        task = coroutine(*args, **kwargs)
        asyncio.get_event_loop().run_until_complete(task)

    return decorator
