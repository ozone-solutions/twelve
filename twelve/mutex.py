from asyncio import Lock
from typing import Generic, TypeVar

T = TypeVar('T')


class Mutex(Generic[T]):
    """A wrapper around `asyncio.Lock` with provided data.

    The Mutex is very much like `Lock`, except that data is returned on
    `async with` uses and the `acquire` method.

    Examples:
        You might do this with Lock:
        >>> from asyncio import Lock
        >>> lock = Lock()
        >>> my_data = {'abc': 1}
        >>> await lock.acquire()
        >>> print(my_data['abc'])
        >>> lock.release()

        But with a Mutex, you do:
        >>> from twelve import Mutex
        >>> mutex = Mutex({'abc': 1})
        >>> async with mutex as data:
        ...     print(data['abc'])
    """

    __slots__ = ['_data', '_lock']

    _data: T
    _lock: Lock

    def __init__(self, data: T):
        """Creates a new Mutex, probably with data.

        Examples:
            >>> from twelve import Mutex
            >>> mutex = Mutex({})
            >>> async with mutex as data:
            ...     print(data['abc'])

        Args:
            data: The data to be wrapped in the mutex, accessible only through
                acquiring the mutex.
        """

        self._lock = Lock()
        self._data = data

    async def acquire(self) -> T:
        """Like `asyncio.Lock.release`, but also returns the inner data.

        Works the same, raises the same exceptions, but returns the given data
        after all of that.

        Returns:
            T: The data given in the initializer.
        """

        await self._lock.acquire()

        return self._data

    def locked(self) -> bool:
        """Returns whether the mutex is acquired."""

        return self._lock.locked()

    def release(self) -> None:
        """Release the mutex."""

        self._lock.release()

    async def __aenter__(self) -> T:
        return await self.acquire()

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> None:
        await self._lock.__aexit__(exc_type, exc_val, exc_tb)

    def __repr__(self) -> str:
        state = 'locked' if self.locked() else 'unlocked'

        return f'<twelve.Mutex [{state}]>'
