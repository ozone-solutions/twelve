from typing import Callable, Generator, Iterable, List, TypeVar

T = TypeVar('T')


def all_f(func: Callable[[T], bool], it: Iterable) -> bool:
    """A short-circuiting mapping `all` function.

    Like `all`, but it will call a given function to work over the elements of
    an iterator.

    The main benefit of this is when you need to do some work on an iterator
    prior to checking that that it also checks the predicate.

    This is up to O(n).

    Example:
        Although inefficient, check that all numbers in an array, when converted
        to strings, are only 1 character long.
        >>> all_f(lambda x: len(str(x)) == 1, [1, 10, 2, 3])
        >>> False # This short-circuited after operating on 1 and 10.
        >>> all_f(lambda x: x > 5, [6, 7, 10])
        >>> True

    Args:
        func: The function to call on every element in the iterable.
        it: The iterable to perform predicate checks on.

    Returns:
        Whether all of the elements in the iterable match the predicate provided
        as `func`.
    """

    for item in it:
        if not func(item):
            return False

    return True


def any_f(func: Callable[[T], bool], it: Iterable[T]) -> bool:
    """A short-circuiting mapping `any` function.

    Like `any`, but it will call a given function to work over the elements of
    an iterator.

    The main benefit of this is when you need to do some work on an iterator
    prior to checking that it also checks the predicate.

    This is up to O(n).

    Example:
        >>> any_f(lambda x: x > 5, [1, 6, 10, 11])
        >>> True # Short-circuits after operating over 1 and 6.

    Args:
        func: The function to call on every element in the iterable until a
            predicate matches.
        it: The iterable to perform predicate checks on.

    Returns:
        Whether any (at least one) of the elements in the iterable match the
        predicate provided as `func`.
    """

    for item in it:
        if func(item):
            return True

    return False


def chunk(iterable: Iterable[T], size: int) -> Generator[List[T], None, None]:
    """Chunks an iterable into lists of a given size.

    For example, if a list of `[0, 1, 2, 3, 4]` is chunked into sizes of 2, then
    a generator is returned that will yield `[0, 1]`, `[2, 3]`, and `[4]`.

    Examples:
        >>> import twelve
        >>> print(*twelve.chunk(range(5), 2))
        [0, 1] [2, 3] [4]

    Args:
        iterable: The iterable of values to chunk into yielded lists.
        size: The size of each chunk to yield.

    Returns:
        A Generator yielding chunks of the iterable.

    Yields:
        Chunks of the iterable up to the given size.
    """

    buf = []

    for item in iterable:
        buf.append(item)

        if len(buf) == size:
            yield buf

            # nb: don't clear the buffer here, be sure to set another buffer in its
            # place.
            #
            # If we clear the buffer and re-use it, then we'll be re-using the buffer
            # and returning pointers to the same list. In asynchronous scenarios where
            # these buffers may be passed to background tasks, this may result in many
            # of the tasks unknowingly being given the same buffer.
            buf = []

    if buf:
        yield buf
