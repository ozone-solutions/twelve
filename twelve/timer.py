from __future__ import annotations
from datetime import datetime, timedelta
import time
from typing import Optional, Union


class Timer:
    """A timer which can be stopped and used to measure elapsed time."""

    __slots__ = ['_start', '_stop']

    _start: float
    _stop: Optional[float]

    def __init__(self) -> None:
        self._start = time.time()
        self._stop = None

    @property
    def started_time(self) -> float:
        """When the timer was created and thus "started"."""

        return self._start

    @property
    def stopped_time(self) -> Optional[float]:
        """When the timer was stopped, if at all."""

        return self._stop

    @property
    def stopped(self) -> bool:
        """Whether the timer has been stopped."""

        return self._stop is not None

    def elapsed(self) -> float:
        if self._stop is None:
            return time.time() - self._start
        else:
            return self._stop - self._start

    def diff(self, other: Union[datetime, float, int, Timer, timedelta]) -> float:
        """Diffs this Timer from another time representation.

        This will take the "timer" represented by the other object, and subtract the
        elapsed value of this timer from it.

        Examples:
            >>> from twelve import Timer
            >>> timer = Timer()
            >>> time.sleep(1)
            >>> timer.diff(3)
            2.0  # (more or less)

        Args:
            other (Union[datetime, float, int, Timer, timedelta]): The other object to
                diff this Timer's elapsed time from.

        Returns:
            The time difference between this timer and the other object given.

        Raises:
            TypeError: When the given type is not a `datetime.datetime`, `float`, `int`,
                `Timer`, or `datetime.timedelta`
        """

        try:
            # noinspection PyTypeChecker
            return {
                datetime: lambda o: self.elapsed() - (datetime.now() - o).total_seconds(),
                float: lambda o: o - self.elapsed(),
                int: lambda o: float(o) - self.elapsed(),
                Timer: lambda o: o.elapsed() - self.elapsed(),
                timedelta: lambda o: o.total_seconds() - self.elapsed(),
            }[type(other)](other)
        except KeyError:
            raise TypeError("'other' is not a datetime, float, int, or timer")

    def stop(self) -> Optional[float]:
        """Stops the timer, returning the elapsed time if it was still returning.

        Returns None when the timer was already stopped.

        Returns:
            If the timer was still running, the elapsed time will be returned, otherwise
            None will be returned.
        """

        if self._stop is not None:
            return None

        self._stop = time.time()

        return self.elapsed()
