import contextlib
from os import environ


class BaseEnvironment:
    """A base Environment class meant to be superclassed.

    The `BaseEnvironment` is a base class meant to be a subclass. The `__init__`
    method reads a list of environment variables defined by the annotations of
    the superclass.

    The current annotation types are supported:

    - bytes
    - bool
    - float
    - int
    - str
    - typing.Optional[T]

    Environments also support private properties. Properties prefixed with `_`
    will have the underscore stripped when searching for the environment
    variable.

    Example:
        >>> from typing import Optional
        >>>
        >>> from twelve import BaseEnvironment
        >>>
        >>> class MyEnvironment(BaseEnvironment):
        ...     log_level: str
        ...     may_not_exist: Optional[bool]
        ...     other_env_var: int
        ...
        >>> env = MyEnvironment()
        >>> env.other_env_var
        7
        >>> env.may_not_exist
        None
        >>> class PrivateProperty(BaseEnvironment):
        ...     _path: str
        ...     @property
        ...     def path(self) -> str:
        ...         return self._path
        ...
        >>> assert PrivateProperty().path is not None
    """

    def __init__(self):
        """Creates a type-safe instance representing the environment variables.

        Pulls all of the environment variables from the list of annotations
        defining the environment, raising `KeyError` if one does not exist.

        Example:
            >>> from twelve import BaseEnvironment
            >>> # Empty environments work.
            >>> class MyEnvironment(BaseEnvironment):
            ...     pass
            ...
            >>> empty_env = MyEnvironment()
            >>> # An Environment's variables are defined by typed annotations.
            >>> class MyTypedEnvironment(BaseEnvironment):
            ...     authorization: str
            ...     log_level: str
            ...
            >>> my_env = MyTypedEnvironment()
            >>> my_env.log_level
            'DEBUG'

        Raises:
            KeyError: When a required environment variable isn't present.
            ValueError: When an environment variable couldn't be cast to its
                required type.
        """

        # If a `dotenv` implementation is installed, then load a `.env` file if
        # available. If not, that's fine, we don't need to.
        with contextlib.suppress(ImportError):
            __import__('dotenv').load_dotenv()

        try:
            annotations = self.__annotations__
        except AttributeError:
            return

        for env_name, annotation in annotations.items():
            self._var(env_name, annotation)

    def _var(self, name: str, annotation) -> None:
        """

        Args:
            name: The name of the environment variable.
            annotation: The class property annotation info.

        Raises:
            KeyError: When a required environment variable isn't present.
            ValueError: When an environment variable couldn't be cast to its
                required type.
        """

        key_name = name[1:].upper() if name.startswith('_') else name.upper()

        try:
            env_value = environ[key_name]
        except KeyError:
            with contextlib.suppress(AttributeError):
                if type(None) in annotation.__args__:
                    setattr(self, name, None)

                    return

            raise

        # Check if the annotation is of a 'primitive'.
        if annotation in (bool, bytes, float, int, str):
            setattr(self, name, annotation(env_value))

            return

        # Otherwise, loop through the possible annotation types and try to cast
        # it.
        for arg in annotation.__args__:
            setattr(self, name, arg(env_value))

            return
