from datetime import datetime, timedelta
import time
from unittest import TestCase

from twelve import Timer


class TestTimer(TestCase):
    def test_elapsed(self) -> None:
        timer = Timer()
        timer._start -= 5

        assert 6 > timer.elapsed() > 4

    def test_diff(self) -> None:
        datetime_now = datetime.now()
        timer = Timer()
        timer._start -= 5
        other_timer = Timer()
        other_timer._start -= 10

        # datetime
        assert 6 > timer.diff(datetime_now) > 3
        # float
        assert 6 > timer.diff(10.) > 3
        # int
        assert 6 > timer.diff(10.) > 3
        # Timer
        assert 6 > timer.diff(other_timer) > 3
        # timedelta
        assert 6 > timer.diff(timedelta(seconds=10)) > 3

    def test_stop(self) -> None:
        timer = Timer()
        # Elapsed time is returned when the timer is stopped after running.
        elapsed_time = timer.stop()
        self.assertIsNotNone(elapsed_time)

        # This is also equal to the elapsed time.
        self.assertEqual(elapsed_time, timer.elapsed())

        # Stopping it a second time returns None. And a third time. And a fourth.
        for _ in range(3):
            self.assertIsNone(timer.stop())

    def test_stopped(self) -> None:
        timer = Timer()
        self.assertFalse(timer.stopped)
        timer.stop()
        self.assertTrue(timer.stopped)

    def test_time_props(self) -> None:
        now = time.time()
        timer = Timer()
        self.assertGreaterEqual(timer.started_time, now)
        timer.stop()
        self.assertIsNotNone(timer.stopped_time)
        self.assertGreaterEqual(timer.stopped_time, now)
        self.assertGreaterEqual(timer.stopped_time, timer.started_time)
