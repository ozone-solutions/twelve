import asyncio
from typing import List
from unittest import TestCase

from twelve import async_test, iterables


class TestIterables(TestCase):
    def test_all_f(self):
        self.assertTrue(iterables.all_f(lambda x: x > 5, [6, 7, 8]))
        self.assertFalse(iterables.all_f(lambda x: x > 5, [5, 6, 7]))

        # Match `all` behaviour on empty lists.
        self.assertTrue(all([]))
        self.assertTrue(iterables.all_f(lambda x: True, []))

    def test_any_f(self):
        self.assertTrue(iterables.any_f(lambda x: x > 5, [1, 2, 6]))
        self.assertFalse(iterables.any_f(lambda x: x > 5, [1]))

        # Match `any` behaviour on empty lists.
        self.assertFalse(any([]))
        self.assertFalse(iterables.any_f(lambda x: True, []))

    def test_chunk(self) -> None:
        numbers = range(0, 20)

        gen = iterables.chunk(numbers, 9)
        self.assertEqual(list(range(9)), next(gen))
        self.assertEqual(list(range(9, 18)), next(gen))
        self.assertEqual([18, 19], next(gen))

        with self.assertRaises(StopIteration):
            next(gen)

    @async_test
    async def test_chunk_in_background(self) -> None:
        async def bg(items: List[int]) -> None:
            # Here we check that the buffer isn't cleared for each yield, which would
            # give us empty lists after yielding.
            self.assertTrue(len(items) == 5)

        await asyncio.gather(*map(bg, iterables.chunk(range(20), 5)))

    def test_chunk_readme_example(self) -> None:
        gen = iterables.chunk(range(13), 6)

        assert next(gen) == [0, 1, 2, 3, 4, 5]
        assert next(gen) == [6, 7, 8, 9, 10, 11]
        assert next(gen) == [12]
