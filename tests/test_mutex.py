import asyncio
from unittest import TestCase

from twelve import Mutex, async_test


class TestMutex(TestCase):
    @async_test
    async def test_data_properly_given(self) -> None:
        given = 'some value'

        mutex: Mutex[str] = Mutex(given)

        async with mutex as data:
            self.assertEqual(given, data)

    @async_test
    async def test_locked(self) -> None:
        mutex = Mutex(None)

        self.assertFalse(mutex.locked())

        async with mutex:
            self.assertTrue(mutex.locked())

        self.assertFalse(mutex.locked())

    @async_test
    async def test_acquire_and_release(self) -> None:
        mutex = Mutex(None)

        self.assertIsNone(await mutex.acquire())
        self.assertTrue(mutex.locked())
        self.assertIsNone(mutex.release())
        self.assertFalse(mutex.locked())

    @async_test
    async def test_repr(self) -> None:
        mutex = Mutex(None)

        self.assertEqual(repr(mutex), '<twelve.Mutex [unlocked]>')

        async with mutex:
            self.assertEqual(repr(mutex), '<twelve.Mutex [locked]>')

    def test_loop(self) -> None:
        loop = asyncio.get_event_loop()

        self.assertEqual(Mutex(None, loop=loop).loop, loop)
