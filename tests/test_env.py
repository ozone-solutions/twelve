from typing import Optional
from unittest import TestCase

from twelve.env import BaseEnvironment


class TestEnv(TestCase):
    def test_attributes(self) -> None:
        class Foo(BaseEnvironment):
            path: str

        self.assertEqual(1, len(Foo.__annotations__))
        foo = Foo()
        self.assertLess(0, len(foo.path))

    def test_no_annotations(self) -> None:
        class Foo(BaseEnvironment):
            pass

        Foo()

    def test_optional_var(self) -> None:
        class Foo(BaseEnvironment):
            path: Optional[str]
            var_not_here: Optional[int]

        foo = Foo()
        self.assertIsNotNone(foo.path)
        self.assertIsNone(foo.var_not_here)

    def test_private_property(self) -> None:
        class Foo(BaseEnvironment):
            _path: str

            @property
            def path(self) -> str:
                return self._path

        self.assertTrue(len(Foo().path) > 0)

    def test_key_public_not_exists(self) -> None:
        class Foo(BaseEnvironment):
            bar_baz: bool

        with self.assertRaises(KeyError):
            Foo()

    def test_key_private_not_exists(self) -> None:
        class Foo(BaseEnvironment):
            _bar_baz: bool

        with self.assertRaises(KeyError):
            Foo()
