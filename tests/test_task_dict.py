import asyncio
from asyncio import CancelledError
from unittest import TestCase

from twelve import TaskDict, async_test


async def foo() -> None:
    await asyncio.sleep(0.5)


class TestTaskDict(TestCase):
    @async_test
    async def test_clear(self) -> None:
        d = TaskDict()
        task1 = asyncio.create_task(foo())
        task2 = asyncio.create_task(foo())
        d[1] = task1
        d[2] = task2

        self.assertTrue(1 in d and 2 in d)
        self.assertTrue(len(d) == 2)

        # Clearing cancels all tasks
        d.clear()

        self.assertTrue(not d)

        with self.assertRaises(CancelledError):
            await task1

    @async_test
    async def test_deleting(self) -> None:
        d = TaskDict()
        task = asyncio.create_task(foo())
        d[1] = task
        self.assertTrue(1 in d)
        self.assertTrue(len(d) == 1)

        del d[1]

        self.assertTrue(not d)

        # Deleting from the dict cancels the task.
        with self.assertRaises(CancelledError):
            await task

    @async_test
    async def test_setting(self) -> None:
        d = TaskDict()
        task = asyncio.create_task(foo())
        d[1] = task

        self.assertTrue(len(d) == 1)

        # Set it again. The original task will now cancel.
        d[1] = asyncio.create_task(foo())

        with self.assertRaises(CancelledError):
            await task

        self.assertTrue(1 in d)
        self.assertTrue(len(d) == 1)
