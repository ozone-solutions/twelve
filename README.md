![CircleCI][circleci] ![BitBucket Issues Open][bitbucket-issues] ![Python][python] ![Black][black]

# twelve

A set of nifty utility functions for working with Python. Sort of like the swiss
knife of your Python toolset.

# Items

## async_test

`async_test` is a decorator meant to be used over asynchronous test functions.
This takess your asynchronous function and makes it synchronous by blocking the
asyncio event loop.

Used like so:

```python
from unittest import TestCase

from twelve import async_test


class TestFoo(TestCase):
    @async_test
    async def test_bar(self) -> None:
        # Asynchronous code here.
        pass
```

## BaseEnvironment

A statically typed class which maps its attributes to variables in the
environment. It's easier to visualize than to describe:

```python
from typing import Optional

from twelve import BaseEnvironment


class Environment(BaseEnvironment):
    interval: Optional[int]
    path: str


my_env = Environment()
print(my_env.path)  # 'your path here'
```

Assuming that `PATH` is in the environment, this will succeed and `interval` may
or may not be `None` depending on whether it's in your environment.

If a key can't be converted into its correct type from a string, then a
`TypeError` is raised. If a key doesn't exist when it's required, then
`KeyError` is raised.

The `BaseEnvironment` supports private attributes, which allows you to create
`@property`s on top of it with real logic.

Here's a good use case:

```python
from pathlib import Path

from twelve import BaseEnvironment


class Environment(BaseEnvironment):
    _local_file_cache_path: str

    def local_file_path(self, filename: str) -> Path:
        return Path(self._local_file_cache_path) / filename
```

## Iterables

Included are a set of lazy functions modeled over their standard library
equivalents.

### all_f

`all_f` is a lazy function which iterates much like `all`, checking if all of
the given values results in `True` with the given lambda.

The difference is that this function short-circuits, unlike its standard library
equivalent.

Similarly is `any_f`, which is a lazy function over `any`.

Below are a couple of visuals. The first checks if all values are greater than
5. We know when operating over only the first value already that this is not
true, but using the standard library's `all` we can't know this and will end up
operating over every value in the iterable.

Second is a visual over `any`, which has the same problems:

```python
# Standard library:
print(all(x > 5 for x in range(10)))
print(any(x > 5 for x in range(10)))

# Twelve:
from twelve import all_f, any_f

# Short-circuits, returning False, after only the first item since it is False.
print(all_f(lambda x: x > 5, range(10)))

# Short-circuits, returning True, after the 6th item since it is True.
print(any_f(lambda x: x > 5, range(10)))
```

### chunk

`chunk` is a function which accepts an iterable of items and a size, and then
returns a generator yielding lists with the requested size. The last list
yielded may have fewer items if the number of items in the iterable is not
divisible by the requested size (e.g. if the `iterable` contains 13 elements
with a requested `size` of 4).

Here's an example:

```python
import twelve

# Chunk a range of 0 to 13, exclusively, with a size of 6.
gen = twelve.chunk(range(13), 6)

assert next(gen) == [0, 1, 2, 3, 4, 5]
assert next(gen) == [6, 7, 8, 9, 10, 11]
assert next(gen) == [12]
```

## Mutex

The `Mutex` is an asynchronous lock containing data, which must be acquired in
order to be accessed. This is like using `asyncio.Lock` and a secondary data
source, but brought together.

Here's examples of how to use the standard library's `asyncio.Lock` to mutex
data behind it, and `twelve`'s method of doing it:

Standard library:

```python
from asyncio import Lock

my_data = {'foo': 1}
lock = Lock()

async with lock:
    print(my_data['foo'])
```

`twelve`:

```python
from twelve import Mutex

mutex = Mutex({'foo': 1})

async with mutex as data:
    print(data['foo'])
```

## TaskDict

A dictionary which tracks `asyncio` tasks and cancels and clears them when
removed or replaced.

When a key is deleted from the `TaskDict`, then the correlated task will be
cancelled. Similarly, when a key's value is set, then if there is already a
key present then its task will be cancelled.

This can be easily illustrated:

```python
import asyncio

from twelve import TaskDict

tasks = TaskDict()
tasks[100] = asyncio.create_task(asyncio.sleep(1))

# Now, when we delete the value, the `asyncio.sleep` operation will be
# cancelled.
del tasks[100]

# This, similarly, happens when the key is updated with a new task:
tasks[200] = asyncio.create_task(asyncio.sleep(1))
tasks[200] = asyncio.create_task(asyncio.sleep(2))

# The `asyncio.sleep(1)` is cancelled and will never finish, but
# `asyncio.sleep(2)` will go on to be successfully finished.
```

[bitbucket-issues]: https://img.shields.io/bitbucket/issues/tristratagroup/twelve?style=for-the-badge
[black]: https://img.shields.io/badge/code%20style-black-%23000000?style=for-the-badge
[circleci]: https://img.shields.io/circleci/build/bitbucket/tristratagroup/twelve?style=for-the-badge
[python]: https://img.shields.io/badge/python-3.7%2C3.8-blue?style=for-the-badge
